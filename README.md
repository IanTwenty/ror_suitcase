# README

This is a [Rigs of Rods](https://www.rigsofrods.org/) mod that adds a suitcase.

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [To Do](#to-do)
* [Install](#install)
* [Usage](#usage)

<!-- vim-markdown-toc -->

## To Do

* WAITING: Improve dev-test cycle
  * [Reload last spawned vehicle does not work under linux · Issue #3099 · RigsOfRods/rigs-of-rods](https://github.com/RigsOfRods/rigs-of-rods/issues/3099)
* Check we can stack them up and drive into them. Deformable.
* Rename untitled.png to suitcase.png. Have to change the .mesh in Blender.
  Maybe we can just import the mesh, edit and export.
* I'd like to be able to pick up the case with the mouse.
* Better texture would be good, more realistic.
* Add some skins for people to choose
* Any way to make him carry the case?
* Future
  * Cases can open
  * Cases can take damage/fly open
  * Stuff inside them can fly out?
  * Wheelie cases
  * Driveable wheelie case
  * Offroad wheelie case

## Install

In future we'll release this to the RoR public repository so it can be easily
installed in-game.

Prerequisites:

* [ninja](https://ninja-build.org/)

Clone this repo. Create a file `build_config.ninja` with the settings:

```ninja
moddir=destination mod dir for your RoR install e.g. ~/.rigsofrods/mods
builddir=where to stick build artifacts e.g. /tmp/blah
```

Then run the build (`ninja`) and the mod will be installed into the above mods
dir for your RoR.

## Usage

You'll find a new load in your spawn menu "Suitcase".
